package cn.appinfo.service.backenduserbiz;

import org.springframework.stereotype.Service;

import cn.appinfo.pojo.BackendUser;

@Service
public interface BackenduserService {

	public BackendUser findBackUser(BackendUser buser);
	
	
}
