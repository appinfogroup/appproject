package cn.appinfo.dao.backenduser;

import org.springframework.stereotype.Repository;

import cn.appinfo.pojo.BackendUser;

@Repository
public interface BackendUserDao {

	public BackendUser getBuserByCode(BackendUser buser);
	
	
	
}
